# syntax = docker/dockerfile:experimental

FROM python:3.10-slim as base

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8 \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    VENV_PATH="/opt/venv" \
    PATH="/opt/venv/bin:$PATH"

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        git make \
        curl ca-certificates gnupg apt-transport-https

FROM base as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc python-dev bison git

RUN --mount=type=bind,target=./pyproject.toml,src=./pyproject.toml \
    --mount=type=bind,target=./poetry.lock,src=./poetry.lock \
    --mount=type=cache,target=/root/.cache/pypoetry \
    python -m venv /opt/venv && \
    pip3 install --upgrade pip && \
    pip3 install poetry && \
    poetry install

FROM base

ARG UID=1000
ARG GID=1000

COPY --from=builder /opt/venv/ /opt/venv/

RUN mkdir -p /app && \
    useradd -d /app -r -u ${UID} app && \
    chown app:${GID} -R /app && \
    mkdir -p "/app"

USER app:${GID}
WORKDIR /app
